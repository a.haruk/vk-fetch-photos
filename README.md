# VK fetch photos

This script use python 3.7 and [aiohttp](https://docs.aiohttp.org/en/stable/) for download
vk avatar image.

## Example


```shell
python main.py ~/PycharmProjects/vk_fetch_photos/run_data/proxies.txt /tmp/out/ 0 3 1000

```